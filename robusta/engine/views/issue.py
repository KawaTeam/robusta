# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.views.generic import DetailView

# Robusta
from robusta.engine.models.comment import Comment
from robusta.engine.models.issue import Issue


# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class IssueView(DetailView):
    model = Issue
    template_name = 'issue.html'

    slug_url_kwarg = 'issue'

    query_pk_and_slug = False

    def get_object(self, queryset=None):
        """ Retrieve additional informations
        """

        item = super().get_object(queryset)
        item.comments = Comment.objects.filter(issue__slug=item.slug)

        return item
