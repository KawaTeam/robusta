# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.urls import include, path

# Robusta
from robusta.engine.views.issue import IssueView
from robusta.engine.forms.issue import IssueCreate, IssueUpdate, IssueDelete
from robusta.engine.views.project import ProjectView, ProjectsView


# ------------------------------------------------------------------------------
#   Patterns
# ------------------------------------------------------------------------------

urlpatterns = [
    path(r'projects/', include([
        path(r'',
             ProjectsView.as_view(),
             name='projects-list'),
        path(r'<str:project>/',
             ProjectView.as_view(),
             name='project-detail'),
        path(r'<str:project>/<str:slug>-<int:pk>/',
             IssueView.as_view(),
             name='issue-detail'),
    ])),
    path(r'add/', include([
        path(r'issue/',
             IssueCreate.as_view(),
             name='action-add-issue'),
    ])),
    path(r'update/', include([
        path(r'issue/<int:pk>/',
             IssueUpdate.as_view(),
             name='action-update-issue'),
    ])),
    path(r'delete/', include([
        path(r'issue/<int:pk>/',
             IssueDelete.as_view(),
             name='action-delete-issue'),
    ])),
]
