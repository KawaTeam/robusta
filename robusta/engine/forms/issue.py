# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin)

from django.urls import reverse_lazy

from django.views.generic.edit import CreateView, DeleteView, UpdateView

# Robusta
from robusta.engine.models.issue import Issue


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class IssueCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Issue
    template_name = 'create/issue.html'

    slug_url_kwarg = 'issue'

    permission_required = [
        'issue.can_add',
    ]

    fields = [
        'project',
        'title',
        'summary',
        'content',
        'due_date',
        'working_time',
        'resolver',
        'level',
        'status',
        'tags',
    ]

    def form_valid(self, form):
        """
        """

        form.instance.reporter = self.request.user
        form.instance.updater = self.request.user

        return super().form_valid(form)


class IssueUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Issue
    template_name = 'create/issue.html'

    slug_url_kwarg = 'issue'

    permission_required = [
        'issue.can_update',
    ]

    fields = [
        'project',
        'title',
        'summary',
        'content',
        'due_date',
        'working_time',
        'resolver',
        'level',
        'status',
        'tags',
    ]

    def has_permission(self):
        """
        """

        item = self.get_object()

        if item is not None:

            if not self.request.user == item.reporter:
                return False

        return self.request.user.has_perms(self.get_permission_required())

    def form_valid(self, form):
        """
        """

        form.instance.updater = self.request.user

        return super().form_valid(form)


class IssueDelete(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Issue
    template_name = 'delete/issue.html'

    slug_url_kwarg = 'issue'

    permission_required = [
        'issue.can_delete',
    ]

    def get_success_url(self):
        """ Retrieve the sucess URL when an issue was deleted
        """

        return reverse_lazy('project-detail', kwargs={
            'project': self.object.project.slug,
        })
