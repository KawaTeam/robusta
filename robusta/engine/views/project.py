# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.views.generic import DetailView, ListView

# Robusta
from robusta.engine.models.project import Project
from robusta.engine.models.issue import Issue


# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class ProjectsView(ListView):
    model = Project
    template_name = 'projects.html'


class ProjectView(DetailView):
    model = Project
    template_name = 'project.html'

    slug_url_kwarg = 'project'

    query_pk_and_slug = False

    def get_object(self, queryset=None):
        """ Retrieve additional informations
        """

        item = super().get_object(queryset)
        item.issues = Issue.objects.filter(project__slug=item.slug)

        return item
