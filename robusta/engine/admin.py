# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.contrib import admin

# Robusta
from robusta.engine.models.color import Color
from robusta.engine.models.comment import Comment
from robusta.engine.models.file import File
from robusta.engine.models.issue import Issue
from robusta.engine.models.level import Level
from robusta.engine.models.project import Project
from robusta.engine.models.status import Status
from robusta.engine.models.tag import Tag


# ------------------------------------------------------------------------------
#   Manager
# ------------------------------------------------------------------------------

admin.site.register(Color)
admin.site.register(Comment)
admin.site.register(File)
admin.site.register(Issue)
admin.site.register(Level)
admin.site.register(Project)
admin.site.register(Status)
admin.site.register(Tag)
