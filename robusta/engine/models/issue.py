# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.urls import reverse

from django.contrib.auth.models import User

from django.utils.translation import gettext as _

# Robusta
from robusta.engine.models.abstract import Abstract
from robusta.engine.models.level import Level
from robusta.engine.models.project import Project
from robusta.engine.models.status import Status
from robusta.engine.models.tag import Tag


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Issue(Abstract):

    title = models.CharField(max_length=96, verbose_name=_('Title'))
    summary = models.CharField(max_length=512, verbose_name=_('Summary'))

    content = models.TextField(null=True,
                               blank=True,
                               verbose_name=_('Content'))

    due_date = models.DateTimeField(null=True,
                                    blank=True,
                                    verbose_name=_('Due date'))

    working_time = models.TimeField(null=True,
                                    blank=True,
                                    verbose_name=_('Working time'))

    reporter = models.ForeignKey(User,
                                 on_delete=models.CASCADE,
                                 related_name='issue_reporter',
                                 verbose_name=_('Reporter'))

    resolver = models.ForeignKey(User,
                                 null=True,
                                 blank=True,
                                 on_delete=models.CASCADE,
                                 related_name='issue_resolver',
                                 verbose_name=_('Resolver'))

    updater = models.ForeignKey(User,
                                on_delete=models.CASCADE,
                                related_name='issue_updater',
                                verbose_name=_('Updater'))

    project = models.ForeignKey(Project,
                                on_delete=models.CASCADE,
                                verbose_name=_('Project'))

    level = models.ForeignKey(Level,
                              on_delete=models.CASCADE,
                              verbose_name=_('Level'))

    status = models.ForeignKey(Status,
                               on_delete=models.CASCADE,
                               verbose_name=_('Status'))

    tags = models.ManyToManyField(Tag,
                                  blank=True,
                                  verbose_name=_('Tags'))

    class Meta(Abstract.Meta):
        # Translator: This is the Issue model verbose names
        verbose_name = _('Issue')
        verbose_name_plural = _('Issues')

    def get_absolute_url(self):
        """ Retrieve absolute URL for current issue
        """

        return reverse('issue-detail', kwargs={
            'pk': self.pk,
            'project': self.project.slug,
            'slug': self.slug,
        })
