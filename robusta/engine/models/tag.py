# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.utils.translation import gettext as _

# Robusta
from robusta.engine.models.abstract import Abstract
from robusta.engine.models.color import Color


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Tag(Abstract):

    name = models.CharField(max_length=96, verbose_name=_('Name'))

    color = models.ForeignKey(Color,
                              on_delete=models.CASCADE,
                              blank=True,
                              related_name="tag_color",
                              verbose_name=_('Color'))

    class Meta(Abstract.Meta):
        # Translator: This is the Tag model verbose names
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')
