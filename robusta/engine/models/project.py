# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.contrib.auth.models import User

from django.utils.translation import gettext as _

# Robusta
from robusta.engine.models.abstract import Abstract
from robusta.engine.models.tag import Tag


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Project(Abstract):

    name = models.CharField(max_length=96,
                            verbose_name=_('Name'),
                            unique=True)

    description = models.TextField(verbose_name=_('Description'))

    owner = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              related_name='project_owner',
                              verbose_name=_('Owner'))

    members = models.ManyToManyField(User,
                                     blank=True,
                                     related_name='project_members',
                                     verbose_name=_('Members'))

    tags = models.ManyToManyField(Tag, blank=True, verbose_name=_('Tags'))

    class Meta(Abstract.Meta):
        # Translator: This is the Project model verbose names
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')
