# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.utils.translation import gettext as _

# Robusta
from robusta.engine.models.abstract import Abstract


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Level(Abstract):

    name = models.CharField(max_length=96, verbose_name=_('Name'))

    rate = models.IntegerField(default=0, verbose_name=_('Rate'))

    class Meta(Abstract.Meta):
        # Translator: This is the Level model verbose names
        verbose_name = _('Level')
        verbose_name_plural = _('Levels')
