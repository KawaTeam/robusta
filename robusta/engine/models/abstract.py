# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Django
from django.db import models

from django.contrib.auth.models import User

from django.utils.text import slugify
from django.utils.translation import gettext as _


# ------------------------------------------------------------------------------
#   Models
# ------------------------------------------------------------------------------

class Abstract(models.Model):

    slug = models.SlugField(null=True, max_length=96, editable=False)

    created_date = models.DateTimeField(auto_now_add=True,
                                        verbose_name=_('Created date'))
    updated_date = models.DateTimeField(auto_now=True,
                                        verbose_name=_('Updated date'))

    class Meta():
        abstract = True

        ordering = ['-updated_date', 'slug']

    def __str__(self):
        """ Represents the child model as string

        Returns
        -------
        str
            Child model representation
        """

        for attribute in ('title', 'name'):
            if hasattr(self, attribute):
                return getattr(self, attribute, 'undefined')

        return f"{self.__class__.__name__} #{self.pk}"

    def save(self, *args, **kwargs):
        """ Override save method to generate a slug field
        """

        self.slug = slugify(str(self))

        super().save(*args, **kwargs)
